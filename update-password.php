<?php

	include 'connection.php';

	# User Validation
	if ( !isset($_REQUEST['user_id']) || $_REQUEST['user_id'] === '' ) {
		$response['content'] = "No user_id sent.";
		echo json_encode($response);
		exit;
	}
	$stmt = $m->prepare("SELECT * FROM `users` WHERE `id` = ?");
	$stmt->bind_param('s', $_REQUEST['user_id']);
	$stmt->execute();
	$stmt->store_result();
	if ( $stmt->num_rows < 0 ) {
		$response['content'] = 'User ID not found.';
		echo json_encode($response);
		exit;
	};
	$stmt->free_result();

	$uid = $_REQUEST['user_id'];

	# Password Validation
	if ( !isset($_REQUEST['new_password']) || $_REQUEST['new_password'] === '' ) {
		$response['content'] = "No new password sent.";
		echo json_encode($response);
		exit;
	}
	if ( strlen($_REQUEST['new_password']) < 4 ) {
		$response['content'] = "New password must be at least 4 characters in length.";
		echo json_encode($response);
		exit;
	}

	# Check Old Password
	if ( !isset($_REQUEST['old_password']) || $_REQUEST['old_password'] === '' ) {
		$response['content'] = "No old password sent.";
		echo json_encode($response);
		exit;
	}
	$stmt = $m->prepare("SELECT `password`,`salt` FROM `users` WHERE `id` = ?");
	$stmt->bind_param('s', $uid);
	$stmt->execute();
	$stmt->bind_result($op, $s);
	$stmt->fetch();
	$vop = $_REQUEST['old_password'];
	if ( md5(md5(substr($s, 0, 4)).md5($vop).md5(substr($s, 4))) != $op ) {
		$response['content'] = 'Old password was incorrect.';
		echo json_encode($response);
		exit;
	}
	$stmt->free_result();

	$np = $_REQUEST['new_password'];
	$salt = uniqid();
	$pw = md5(md5(substr($salt, 0, 4)).md5($np).md5(substr($salt, 4)));

	$stmt = $m->prepare("UPDATE `users` SET `password` = ?, `salt` = ?, `updated_at` = CURRENT_TIMESTAMP WHERE `id` = ?");
	$stmt->bind_param('sss', $pw, $salt, $uid);
	$stmt->execute();
	if ( !$stmt->execute() ) {
		$response['content'] = 'Query error updating password.';
		echo json_encode($response);
		exit;
	}

	$response['status'] = "OK";
	$response['content'] = "Password updated successfully.";
	echo json_encode($response);
	exit;

?>