<?php

	include 'connection.php';

	# User Validation
	if ( !isset($_REQUEST['user_id']) || $_REQUEST['user_id'] === '' ) {
		$response['content'] = "No user_id sent.";
		echo json_encode($response);
		exit;
	}
	$stmt = $m->prepare("SELECT * FROM `users` WHERE `id` = ?");
	$stmt->bind_param('s', $_REQUEST['user_id']);
	$stmt->execute();
	$stmt->store_result();
	if ( $stmt->num_rows == 0 ) {
		$response['content'] = 'User ID not found.';
		echo json_encode($response);
		exit;
	};
	$stmt->free_result();
	$uid = $_REQUEST['user_id'];

	# Type Validation
	if ( !isset($_REQUEST['type']) || $_REQUEST['type'] === '' ) {
		$response['content'] = "No game type sent.";
		echo json_encode($response);
		exit;
	}
	$type = $_REQUEST['type'];
	if ( $type != 'pickems' && $type != 'squares' ) {
		$response['content'] = "Invalid game type sent.";
		echo json_encode($response);
		exit;
	}
	$typeGame = $type.'_game';

	# Game Validation
	if ( !isset($_REQUEST['game_id']) || $_REQUEST['game_id'] === '' ) {
		$response['content'] = "No game_id sent.";
		echo json_encode($response);
		exit;
	}
	$stmt = $m->prepare("SELECT `access_code` FROM `$typeGame` WHERE `id` = ?");
	$stmt->bind_param('s', $_REQUEST['game_id']);
	$stmt->execute();
	$stmt->bind_result($accessCode);
	$stmt->fetch();
	if ( $accessCode == 0 ) {
		$response['content'] = 'Game ID not found.';
		echo json_encode($response);
		exit;
	}
	$stmt->free_result();
	$gid = $_REQUEST['game_id'];

	# Verify Access Code
	if ( $accessCode != $_REQUEST['access_code'] ) {
		$response['content'] = "Incorrect access code provided.";
		echo json_encode($response);
		exit;
	}

	# Check if user has already joined
	$typeAssoc = $type."_assoc";
	$stmt = $m->prepare("SELECT * FROM `$typeAssoc` WHERE `user_id` = ? AND `game_id` = ?");
	$stmt->bind_param('ss', $uid, $gid);
	$stmt->execute();
	$stmt->store_result();
	var_dump($stmt->num_rows);
	if ( $stmt->num_rows > 0 ) {
		$response['content'] = "User has alread joined this ".$type." game.";
		echo json_encode($response);
		exit;
	}
	$stmt->free_result();

	# Create Assoc
	$stmt = $m->prepare("INSERT INTO `$typeAssoc` (`user_id`, `game_id`, `answer_id`, `created_at`) VALUES (?, ?, NULL, CURRENT_TIMESTAMP)");
	$stmt->bind_param('ss', $uid, $gid);
	if ( !$stmt->execute() ) {
		$response['content'] = "Query error adding game assoc.";
		echo json_encode($response);
		exit;
	}

	$response['status'] = "OK";
	$response['content'] = "Game joined successfully.";
	echo json_encode($response);
	exit;


?>