<?php

	include 'connection.php';

	# Username Validation
	if ( !isset($_REQUEST['username']) || $_REQUEST['username'] === '' ) {
		$response['content'] = "No username sent.";
		echo json_encode($response);
		exit;
	}
	$username = $_REQUEST['username'];

	# Password Validation
	if ( !isset($_REQUEST['password']) || $_REQUEST['password'] === '' ) {
		$response['content'] = "No password sent.";
		echo json_encode($response);
		exit;
	}
	if ( strlen($_REQUEST['password']) < 4 ) {
		$response['content'] = "Password must be at least 4 characters in length.";
		echo json_encode($response);
		exit;
	}
	$password = $_REQUEST['password'];
	$stmt = $m->prepare("SELECT `id`,`password`,`salt` FROM `users` WHERE `username` = ?");
	$stmt->bind_param('s', $username);
	$stmt->execute();
	$stmt->bind_result($uid, $pw, $s);
	$stmt->fetch();
	if ( $stmt->num_rows == 0 ) {
		$response['content'] = 'Username not found.';
		echo json_encode($response);
		exit;
	};
	$vop = $_REQUEST['password'];
	if ( md5(md5(substr($s, 0, 4)).md5($password).md5(substr($s, 4))) != $pw ) {
		$response['content'] = 'Incorrect username / password combination.';
		echo json_encode($response);
		exit;
	}

	$response['status'] = "OK";
	$response['content'] = $uid;
	echo json_encode($response);
	exit;

?>