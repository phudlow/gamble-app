<?php

	include 'connection.php';

	# Username Validation
	if ( !isset($_REQUEST['username']) || $_REQUEST['username'] === '' ) {
		$response['content'] = "No username sent.";
		echo json_encode($response);
		exit;
	}
	$stmt = $m->prepare("SELECT * FROM `users` WHERE `username` = ?");
	$stmt->bind_param('s', $_REQUEST['username']);
	$stmt->execute();
	$stmt->store_result();
	if ( $stmt->num_rows > 0 ) {
		$response['content'] = 'Username unavailable.';
		echo json_encode($response);
		exit;
	};

	# Password Validation
	if ( !isset($_REQUEST['password']) || $_REQUEST['password'] === '' ) {
		$response['content'] = "No password sent.";
		echo json_encode($response);
		exit;
	}
	if ( strlen($_REQUEST['password']) < 4 ) {
		$response['content'] = "Password must be at least 4 characters in length.";
		echo json_encode($response);
		exit;
	}

	extract($_REQUEST);

	$salt = uniqid();
	$pw = md5(md5(substr($salt, 0, 4)).md5($password).md5(substr($salt, 4)));

	$stmt = $m->prepare("INSERT INTO `users` (`username`, `password`, `salt`, `display_name`, `created_at`) VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP)");
	$stmt->bind_param('ssss', $username, $pw, $salt, $username);
	if ( !$stmt->execute() ) {
		$response['content'] = 'Query error adding user.';
		echo json_encode($response);
		exit;
	}

	$response['status'] = "OK";
	$response['content'] = $m->insert_id;
	echo json_encode($response);
	exit;

?>