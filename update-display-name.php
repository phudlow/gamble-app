<?php

	include 'connection.php';

	# User Validation
	if ( !isset($_REQUEST['user_id']) || $_REQUEST['user_id'] === '' ) {
		$response['content'] = "No user_id sent.";
		echo json_encode($response);
		exit;
	}
	$stmt = $m->prepare("SELECT * FROM `users` WHERE `id` = ?");
	$stmt->bind_param('s', $_REQUEST['user_id']);
	$stmt->execute();
	$stmt->store_result();
	if ( $stmt->num_rows < 0 ) {
		$response['content'] = 'User ID not found.';
		echo json_encode($response);
		exit;
	};
	$stmt->free_result();

	$uid = $_REQUEST['user_id'];

	# Display Name Validation
	if ( !isset($_REQUEST['display_name']) || $_REQUEST['display_name'] === '' ) {
		$response['content'] = "No display name sent.";
		echo json_encode($response);
		exit;
	}

	$dn = $_REQUEST['display_name'];

	$stmt = $m->prepare("UPDATE `users` SET `display_name` = ? WHERE `id` = ?");
	$stmt->bind_param('ss', $dn, $uid);
	$stmt->execute();
	if ( !$stmt->execute() ) {
		$response['content'] = 'Query error updating display name.';
		echo json_encode($response);
		exit;
	}

	$response['status'] = "OK";
	$response['content'] = "Display name updated successfully.";
	echo json_encode($response);
	exit;

?>