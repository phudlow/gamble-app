<?php

	include 'connection.php';

	# User Validation
	if ( !isset($_REQUEST['user_id']) || $_REQUEST['user_id'] === '' ) {
		$response['content'] = "No user_id sent.";
		echo json_encode($response);
		exit;
	}
	$stmt = $m->prepare("SELECT * FROM `users` WHERE `id` = ?");
	$stmt->bind_param('s', $_REQUEST['user_id']);
	$stmt->execute();
	$stmt->store_result();
	if ( $stmt->num_rows < 0 ) {
		$response['content'] = 'User ID not found.';
		echo json_encode($response);
		exit;
	};
	$stmt->free_result();
	$uid = $_REQUEST['user_id'];

	# Question Validation
	if ( !isset($_REQUEST['question']) || $_REQUEST['question'] === '' ) {
		$response['content'] = "No question sent.";
		echo json_encode($response);
		exit;
	}
	$q = $_REQUEST['question'];

	# Answers Validation
	if ( !isset($_REQUEST['answers']) || count($_REQUEST['answers']) == 0 ) {
		$response['content'] = "No answers sent.";
		echo json_encode($response);
		exit;
	} else {
		$answers = json_decode($_REQUEST['answers']);
		foreach ( $answers as $key => $value ) {
			if ( $value == '' ) {
				$response['content'] = "At least one answer was empty.";
				echo json_encode($response);
				exit;
			}
		}
	}

	# Create random 4 digit access code
	$ac = str_pad(rand(0, pow(10, 4)-1), 4, '0', STR_PAD_LEFT);

 	# Add Game
	$stmt = $m->prepare("INSERT INTO `pickems_game` (`owner_id`, `access_code`, `question`, `created_at`) VALUES (?, ?, ?, CURRENT_TIMESTAMP)");
	$stmt->bind_param('sss', $uid, $ac, $q);
	if ( !$stmt->execute() ) {
		$response['content'] = 'Query error adding pickems game.';
		echo json_encode($response);
		exit;
	}
	$gid = $m->insert_id;

	# Add Answers
	foreach ( $answers as $key => $value ) {
		$stmt = $m->prepare("INSERT INTO `pickems_answer` (`game_id`, `text`, `is_correct`, `created_at`) VALUES (?, ?, 0, CURRENT_TIMESTAMP)");
		$stmt->bind_param('ss', $gid, $value);
		if ( !$stmt->execute() ) {
			$response['content'] = 'Query error adding pickems answer.';
			echo json_encode($response);
			exit;
		}
	}

	# Add Assoc
	$stmt = $m->prepare("INSERT INTO `pickems_assoc` (`user_id`, `game_id`, `answer_id`, `created_at`) VALUES (?, ?, NULL, CURRENT_TIMESTAMP)");
	$stmt->bind_param('ss', $uid, $gid);
	if ( !$stmt->execute() ) {
		$response['content'] = 'Query error adding pickems assoc.'.$stmt->error;
		echo json_encode($response);
		exit;
	}

	$response['status'] = "OK";
	$response['content'] = array(
		"game_id" => $gid,
		"access_code" => $ac,
 	);
	echo json_encode($response);
	exit;


?>