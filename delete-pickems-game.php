<?php

	include 'connection.php';

	# User Validation
	if ( !isset($_REQUEST['user_id']) || $_REQUEST['user_id'] === '' ) {
		$response['content'] = "No user_id sent.";
		echo json_encode($response);
		exit;
	}
	$stmt = $m->prepare("SELECT * FROM `users` WHERE `id` = ?");
	$stmt->bind_param('s', $_REQUEST['user_id']);
	$stmt->execute();
	$stmt->store_result();
	if ( $stmt->num_rows == 0 ) {
		$response['content'] = 'User ID not found.';
		echo json_encode($response);
		exit;
	};
	$stmt->free_result();
	$uid = $_REQUEST['user_id'];

	# Game Validation
	if ( !isset($_REQUEST['game_id']) || $_REQUEST['game_id'] === '' ) {
		$response['content'] = "No game_id sent.";
		echo json_encode($response);
		exit;
	}
	$stmt = $m->prepare("SELECT `owner_id` FROM `pickems_game` WHERE `id` = ?");
	$stmt->bind_param('s', $_REQUEST['game_id']);
	$stmt->execute();
	$stmt->bind_result($ownerId);
	$stmt->fetch();
	if ( $ownerId == 0 ) {
		$response['content'] = 'Game ID not found.';
		echo json_encode($response);
		exit;
	}
	$stmt->free_result();
	$gid = $_REQUEST['game_id'];

	# Ensure game owner and person deleting are the same
	if ( $ownerId != $uid ) {
		$response['content'] = 'Only the game owner may delete a pickems game.';
		echo json_encode($response);
		exit;
	}

	$stmt = $m->prepare("DELETE FROM `pickems_game` WHERE `id` = ?");
	$stmt->bind_param('s', $gid);
	if ( !$stmt->execute() ) {
		$response['content'] = 'Query error deleting pickems game.';
		echo json_encode($response);
		exit;
	};

	$response['status'] = "OK";
	$response['content'] = "Pickems game deleted successfully.";
	echo json_encode($response);
	exit;

?>